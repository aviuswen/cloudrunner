package cloud.test

import cloud.dqn.utilities.CompactUUID
import cloud.dqn.utilities.LongId
import java.util.*

fun main(args: Array<String>) {
    val longId = LongId(4)
    val uuid = UUID.randomUUID()
    val compactUUID = CompactUUID(uuid)

    println("longId(4) = ${longId.str}")
    println("compactUUID($uuid) = ${compactUUID.str}")
}

